<?php

/**
 * 
 */
class Skill_m extends CI_model
{
	
	public function disp_skill()
	{
		return $this->db->get('skill')->result_array();

	}


	public function getbyId($id_skill){
		return $this->db->get_where('skill', ['Id'=>$id_skill])->row_array();
	}

	public function update(){
		$data = [
			"Nama" => $this->input->post('Nama',true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('Skill',$data);
	}

	
	public function hapus($id_skill){
		$this->db->where('id',$id_skill);
		$this->db->delete('Skill');
	}

	public function cari(){
		$result = $this->input->post('cari');
		if ($result!=NULL) {
			return $this->db->get_where('skill', ['Nama'=>$this->input->post('cari',true)])->result_array();

		}

		else{
			return $this->db->get('skill')->result_array();
		}

	}

	public function getHerobyId($id_skill){
		$query = "
		select hero_skill.id as id,hero.Id as hero_id,hero.Nama, '".$id_skill."' as skill_id
		from hero
		inner join hero_skill on hero_skill.id_hero=hero.id
        WHERE hero_skill.id_skill = '".$id_skill."'
		" ;
		return $this->db->query($query)->result_array();
	}

	public function hapusHeroSkill($id_hero_skill){
		$this->db->where('id',$id_hero_skill);
		$this->db->delete('hero_skill');
	}

}

?>

