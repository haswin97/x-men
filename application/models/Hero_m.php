<?php

/**
 * 
 */
class Hero_m extends CI_model
{
	
	public function disp_hero()
	{
		return $this->db->get('hero')->result_array();

	}


	public function getbyId($id_hero){
		return $this->db->get_where('hero', ['Id'=>$id_hero])->row_array();
	}

	public function update(){
		$data = [
			"Nama" => $this->input->post('Nama',true),
			"Jenis_Kelamin" => $this->input->post('Jenis_Kelamin',true),
		];

		$this->db->where('Id', $this->input->post('Id'));
		$this->db->update('Hero',$data);
	}

	
	public function hapus($id_hero){
		$this->db->where('Id',$id_hero);
		$this->db->delete('Hero');
	}

	public function cari(){
		$result = $this->input->post('cari');
		if ($result!=NULL) {
			return $this->db->get_where('hero', ['Nama'=>$this->input->post('cari',true)])->result_array();

		}

		else{
			return $this->db->get('hero')->result_array();
		}

	}

	public function getSkillbyId($id_hero){
		$query = "
		select hero_skill.id as id,skill.id as skill_id,skill.nama,'".$id_hero."' as id_hero
		from skill
		inner join hero_skill on hero_skill.id_skill=skill.id
        WHERE hero_skill.id_hero = '".$id_hero."'
		" ;
		return $this->db->query($query)->result_array();
	}

	public function hapusHeroSkill($id_hero_skill){
		$this->db->where('id',$id_hero_skill);
		$this->db->delete('hero_skill');
	}

	public function tambahHeroSkill(){
		$data = [
			"id_hero" =>  $this->input->post('Id',true),
			"id_skill" => $this->input->post('skill_id',true),
		];

		$this->db->insert('hero_skill',$data);
	}

}

?>

