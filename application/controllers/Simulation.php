<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simulation extends CI_Controller {

	public function __construct(){
		parent::__construct();
		//$this->load->model('Simulation_m');
		$this->load->library('form_validation');
	}

	public function index()
	{
		//$data['hero'] = $this->Hero_m->disp_hero(); 
		$this->load->view('simulation_v');
	}

	public function edit($id_hero){
		$data['hero'] = $this->Hero_m->getbyId($id_hero);
		$this->load->view('detail_v',$data);
	}

	public function update($id_hero){
		$this->Hero_m->update(); 
		redirect('Hero');
	}

	public function del($id_hero){
		$this->Hero_m->hapus($id_hero);
		redirect('Hero',$data);	
	}

	public function search(){
		$data['hero'] = $this->Hero_m->cari();
		$this->load->view('Hero_v',$data);

	}
}
