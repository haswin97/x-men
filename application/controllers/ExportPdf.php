<?php
class ExportPdf extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

    }
 
    public function hero()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetProtection($permissions=array('print', 'copy'), $user_pass='wearehero', $owner_pass=null, $mode=1);
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('');
        $pdf->setTitle('Daftar Hero X-Men');

        // mencetak string 
        $pdf->Cell(190,13,'Daftar Hero',0,1,'C');
        $pdf->SetLeftMargin(40);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('PDFAHelvetica','B',10);
        $pdf->Cell(25,6,'No',1,0);
        $pdf->Cell(70,6,'Nama Hero',1,0);
        $pdf->Cell(35,6,'Jenis Kelamin',1,1);
        $pdf->SetFont('PDFAHelvetica','',10);
        $hero = $this->db->get('hero')->result();
        $i = 1;
        foreach ($hero as $row){
            $pdf->Cell(25,6,$i,1,0);
            $pdf->Cell(70,6,$row->Nama,1,0);
            $pdf->Cell(35,6,$row->Jenis_Kelamin,1,1);
            $i =$i+1;
        }
        $pdf->Output('list-hero.pdf', 'I');
    }


    public function skill()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetProtection($permissions=array('print', 'copy'), $user_pass='wearehero', $owner_pass=null, $mode=1);
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('');
        $pdf->setTitle('Daftar Skill X-Men');

        // mencetak string 
        $pdf->Cell(190,13,'Daftar Skill',0,1,'C');
        $pdf->SetLeftMargin(65);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('PDFAHelvetica','B',10);
        $pdf->Cell(25,6,'No',1,0);
        $pdf->Cell(60,6,'Nama Skill',1,1);
        $pdf->SetFont('PDFAHelvetica','',10);
        $skill = $this->db->get('skill')->result();
        $i = 1;
        foreach ($skill as $row){
            $pdf->Cell(25,6,$i,1,0);
            $pdf->Cell(60,6,$row->Nama,1,1);
            $i =$i+1;
        }
        $pdf->Output('list-skill.pdf', 'I');
    }
 
}