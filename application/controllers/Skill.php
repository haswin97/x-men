<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skill extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Skill_m');
		$this->load->model('Hero_m');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['skill'] = $this->Skill_m->disp_skill();
		$data['hero'] = $this->Hero_m->disp_hero();  
		$this->load->view('skill_v',$data);
	}

	public function edit($id_skill){
		$data['skill'] = $this->Skill_m->getbyId($id_skill);
		$data['hero'] = $this->Skill_m->getHerobyId($id_skill);
		$this->load->view('skill_detail_v',$data);
	}

	public function update($id_skill){
		$this->Skill_m->update(); 
		redirect('skill');
	}

	public function del($id_skill){
		$this->Skill_m->hapus($id_skill);
		redirect('Skill',$data);	
	}

	public function search(){
		$data['Skill'] = $this->Skill_m->cari();
		$this->load->view('Skill_v',$data);

	}

	public function delSkillHero($id_hero_skill,$id_skill){
		$this->Skill_m->hapusHeroSkill($id_hero_skill);
		redirect('Skill/edit/'.$id_skill.'');
	}
}
