<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hero extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Hero_m');
		$this->load->model('Skill_m');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['hero'] = $this->Hero_m->disp_hero(); 
		$data['skill'] = $this->Skill_m->disp_skill(); 
		$this->load->view('Hero_v',$data);
	}

	public function edit($id_hero){
		$data['hero'] = $this->Hero_m->getbyId($id_hero);
		$data['skill'] = $this->Hero_m->getSkillbyId($id_hero);
		$this->load->view('detail_v',$data);
	}

	public function update($id_hero){
		$this->Hero_m->update(); 
		redirect('Hero');
	}

	public function del($id_hero){
		$this->Hero_m->hapus($id_hero);
		redirect('Hero',$data);	
	}

	public function search(){
		$data['hero'] = $this->Hero_m->cari();
		$this->load->view('Hero_v',$data);

	}

	public function delHeroSkill($id_hero_skill,$id_hero){
		$this->Hero_m->hapusHeroSkill($id_hero_skill);
		redirect('Hero/edit/'.$id_hero.'');
	}

	public function addHeroSkill($id_hero){
		$this->form_validation->set_rules('skill_id','Skill Hero','required');
		$this->form_validation->set_rules('coba','Coba','required');


		$data['hero'] = $this->Hero_m->getbyId($id_hero);
		$data['skill'] = $this->Hero_m->getSkillbyId($id_hero);


		if($this->form_validation->run()==false){
			$this->load->view('detail_v',$data);
		}
		
		$this->Hero_m->tambahHeroSkill();
		$this->session->set_flashdata('flash','Berhasil Menambahkan Skill Baru');
		redirect('Skill');
		
	}

}
