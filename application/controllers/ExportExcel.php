<?php
class ExportExcel extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('Hero_m');

    }

    public function hero()
    {
        error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');




// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("Office 2007 XLSX Test Document")
                             ->setSubject("Office 2007 XLSX Test Document")
                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');

// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
}


public function skill()
{
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetProtection($permissions=array('print', 'copy'), $user_pass='wearehero', $owner_pass=null, $mode=1);
    $pdf->AddPage();
        // setting jenis font yang akan digunakan
    $pdf->SetFont('');
    $pdf->setTitle('Daftar Skill X-Men');

        // mencetak string 
    $pdf->Cell(190,13,'Daftar Skill',0,1,'C');
    $pdf->SetLeftMargin(65);
        // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(10,7,'',0,1);
    $pdf->SetFont('PDFAHelvetica','B',10);
    $pdf->Cell(25,6,'No',1,0);
    $pdf->Cell(60,6,'Nama Skill',1,1);
    $pdf->SetFont('PDFAHelvetica','',10);
    $skill = $this->db->get('skill')->result();
    $i = 1;
    foreach ($skill as $row){
        $pdf->Cell(25,6,$i,1,0);
        $pdf->Cell(60,6,$row->Nama,1,1);
        $i =$i+1;
    }
    $pdf->Output('list-hero.pdf', 'I');
}

}