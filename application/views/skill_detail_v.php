<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <title>X-Men</title>
</head>
<body>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        Ini adalah skill <?=$skill['Nama']?>. Skill yang berbahaya. Musuh akan terkejut melihat skill ini.
                    </div>
                    <hr>
                </div>
            </div>

            <a type="button" class="btn btn-success text-white" href="<?=base_url();?>Hero/"><i class="fas fa-arrow-left"></i> Back</a>
            <form action="<?=base_url();?>Skill/update/<?=$skill['id'];?>" method="POST">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Detail Skill: <?=$skill['Nama']?></h3>
                    </div>
                    <div class="col-md-4  text-right">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-edit"></i> Edit</button>
                        <a href="<?=base_url();?>Skill/del/<?= $skill['id'];?>" class="btn btn-danger" onclick= "return confirm('Apakah Skill Ini Akan Dihapus?');" onclick= "return confirm('Apakah Skill Ini Akan Dihapus?');"><i class="fas fa-trash-alt"></i> Delete</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>ID</td>
                                <td><?=$skill['id']?></td>
                                <input name="id" value="<?=$skill['id']?>" hidden>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>
                                    <input type="text" class="form-control" value="<?=$skill['Nama']?>" name="Nama">
                                </td>
                            </tr>
                        </tbody></table>


                        <div class="modal" tabindex="-1" role="dialog" id="exampleModal"> 
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Pilih Skill</label>
                                        <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                                            <?php foreach ($hero as $H): ?>
                                            <option value="<?=$H['id']?>"><?=$H['Nama']?></option>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <table class="table table-bordered">
                            <thead>
                                <tr><th>No</th>
                                    <th>Heroes</th>
                                    <th>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                            <i class="fas fa-plus"></i> Tambah Hero
                                        </button>
                                    </th>
                                </tr></thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($hero as $H): ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $H['Nama']?></td>
                                        <td>
                                            <a href="<?=base_url();?>Skill/delSkillHero/<?= $H['id']?>/<?= $H['skill_id']?>" class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete</a>
                                        </td>
                                    </tr>
                                    <?php $i = $i+1?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
    </html>