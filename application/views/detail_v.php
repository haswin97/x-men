
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
	<title>X-Men</title>
</head>
<body>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">
						Meng-klik "View Detail" di atas akan membawamu ke halaman detail di bawah ini.
						Ini jika kamu mengklik data milik <?= $hero['Nama'] ?>.
					</div>
					<hr>
				</div>
			</div>

			<a type="button" class="btn btn-success text-white" href="<?=base_url();?>Hero/" ><i class="fas fa-arrow-left"></i> Back</a>
			<form action="<?=base_url();?>Hero/update/<?= $hero['Id'];?>" method="post">
				<div class="row">
					<div class="col-md-8">
						<h3>Task #2 Detail Superhero: <?= $hero['Nama'] ?></h3>
					</div>
					<div class="col-md-4  text-right">
						<button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Edit</button>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered">
							<tbody><tr>
								<td>ID</td>
								<td>
									<input type="" name="Id" value="<?= $hero['Id'] ?>" hidden >
									<?= $hero['Id'] ?>

								</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>
									<input type="text" class="form-control" value="<?= $hero['Nama'] ?>" name="Nama" >
								</td>
							</tr>
							<tr>
								<td>Jenis Kelamin</td>
								<td>
									<select class="form-control" name="Jenis_Kelamin">
										<option value="Laki_Laki" selected="">Laki-laki</option>
										<option value="Perempuan">Perempuan</option>
									</select>
								</td>
							</tr>
						</tbody></table>


						<!-- Buku -->
                <div class="makanan m-2 bg-white p-4 mt-4">
                    <?php if($this->session->flashdata('flash')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?=$this->session->flashdata('flash'); ?>
                            <button type="button", class="close", data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php endif;?>   
                    <?php if(validation_errors()) : ?>
                        <div class="alert alert-danger" role="alert"><?=validation_errors()?>
                            
                        </div>
                    <?php endif;?>

					<!-- <?php  var_dump($skill) ?>  -->

							<div class="modal" tabindex="-1" role="dialog" id="exampleModal"> 
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title">Hero Skill</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<p><?= $hero['Nama'] ?></p>
											<form action="<?=base_url()?>Hero/addHeroSkill/<?=$hero['Id']?>" method="post" enctype="multipart/form-data">
											<input name="Id" value="<?= $hero['Id'] ?>" hidden >
											<label class="my-1 mr-2" for="inlineFormCustomSelectPref">Pilih Skill</label>
											<select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="skill_id">
												<?php foreach ($skill as $S): ?>
												<option value="<?=$S['id']?>"><?=$S['nama']?></option>
											<?php endforeach ?>
											</select>
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary">Save changes</button>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
										</form>
									</div>
								</div>
							</div>
						

						<table class="table table-bordered">
							<thead>
								<tr><th>No</th>
									<th>Skill</th>
									<th>
										<!-- Button trigger modal -->
										<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
											<i class="fas fa-plus"></i> Tambah Skill
										</button>
									</th>
								</tr></thead>
								<tbody>
									<?php $i = 1 ?>
									<?php foreach ($skill as $S): ?>
										<tr>
											<td><?= $i ?></td>
											<td><?= $S['nama']?></td>
											<td>
												<a href="<?=base_url();?>Hero/delHeroSkill/<?= $S['id']?>/<?= $S['id_hero']?>" class="btn btn-danger" onclick= "return confirm('Apakah Hero Ini Akan Dihapus?');"><i class="fas fa-minus-circle"></i> Delete</a>
											</td>
										</tr>
										<?php $i = $i+1?>
									<?php endforeach ?>
								</tbody>
							</table>

							<button type="button" class="btn btn-warning"><i class="fas fa-file-excel text-white"></i> Export to Excel</button>
                            <button type="button" class="btn btn-warning"><i class="fas fa-file-pdf text-white" ></i> Export to PDF</button>
                            
						</div>
					</div>

				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
	</html>