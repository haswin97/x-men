
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
	<title>X-Men</title>
</head>
<body>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">
						Di bawah ini adalah Daftar Skill yang ada.<br>
						Kamu bisa mencari nama mereka melalui fasilitas pencarian di sebelah kanan.<br>
					</div>
				</div>
			</div>


			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Hero/">Hero</a>
			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Skill">Skill</a>
			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Simulation">Simulation</a>

			<form>
				<div class="row">
					<div class="col-md-8">
						<h3>Task #5 Daftar Skill</h3>
					</div>
					<div class="col-md-4">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1">
									<i class="fa fa-search"></i>
								</span>
							</div>
							<input type="text" class="form-control" placeholder="Pencarian" aria-describedby="basic-addon1" name="cari" >
							<div class="input-group-append">
								<button class="btn btn-outline-secondary" type="submit">Cari</button>
							</div>
						</div>
					</div>
				</div>	
				<form>
					<!-- <?php var_dump($skill)?> -->
					<div class="row">
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>NO</th>
										<th>Nama</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1 ?>
									<?php foreach ($skill as $S): ?>
										<tr>
											<td><?=$i?></td>
											<td><?=$S['Nama']?></td>
											<td>
												<a href="<?=base_url();?>Skill/edit/<?= $S['id'];?>" class="btn btn-primary"><i class="fas fa-eye"></i> View Detail</a>
											</td>
										</tr>
										<?php $i = $i+1 ?>
									<?php endforeach?>
								</tbody>
							</table>

							<a type="button" class="btn btn-warning"><i class="fas fa-file-excel text-white"></i> Export to Excel</a>
							<a href="<?=base_url();?>ExportPdf/skill" class="btn btn-warning"><i class="fas fa-file-pdf text-white" ></i> Export to PDF</a>
						</div>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		</body>
		</html>