
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
	<title>X-Men</title>
</head>
<body>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">
						Di bawah ini adalah Daftar orang-orang yang super hebat itu.<br>
						Kamu bisa mencari nama mereka melalui fasilitas pencarian di sebelah kanan.<br>
						Kita beruntung memiliki data-data mereka. Jangan sampai jatuh ke tangan musuh, ini akan mengubah dunia..
					</div>
				</div>
			</div>

			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Hero/">Hero</a>
			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Skill">Skill</a>
			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Simulation">Simulation</a>

			<!-- <?php var_dump($hero)?> -->

			<form method="POST" action="<?=base_url();?>Hero/search/">
				<div class="row">
					<div class="col-md-8">
						<h3>Task #1 Daftar Superhero</h3>
					</div>
					<div class="col-md-4">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1">
									<i class="fa fa-search"></i>
								</span>
							</div>
							<input type="text" class="form-control" placeholder="Pencarian" aria-describedby="basic-addon1" name="cari">
							<div class="input-group-append">
								<button class="btn btn-outline-secondary" type="submit">Cari</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ?>
							<?php foreach ($hero as $H): ?>
								<tr>
									<td><?= $i ?></td>
									<td><?= $H['Nama'] ?></td>
									<td><?= $H['Jenis_Kelamin'] ?></td>
									<td>
										<a href="<?=base_url();?>Hero/edit/<?= $H['Id'];?>" class="btn btn-primary"><i class="fas fa-eye"></i> View Detail</a>
										<a href="<?=base_url();?>Hero/del/<?= $H['Id'];?>" class="btn btn-danger" onclick= "return confirm('Apakah Hero Ini Akan Dihapus?');"> <i class="fas fa-user-minus"></i> Delete</a>
									</td>
								</tr>
								<?php $i = $i+1 ?>
							<?php endforeach?>
						</tbody>	
					</table>

					<a href="<?=base_url();?>ExportExcel/hero" type="button" class="btn btn-warning"><i class="fas fa-file-excel text-white"></i> Export to Excel</a>
					<a href="<?=base_url();?>ExportPdf/hero" class="btn btn-warning"><i class="fas fa-file-pdf text-white" ></i> Export to PDF</a>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>

		<!-- <?php var_dump($hero) ?> -->
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>