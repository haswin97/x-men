
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
	<title>X-Men</title>
</head>
<body>
	<div class="row mt-4">
    <div class="col-md-2"></div>
    <div class="col-md-8">

    	<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Hero/">Hero</a>
			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Skill">Skill</a>
			<a type="button" class="btn btn-secondary text-white" href="<?=base_url();?>Simulation">Simulation</a>

        <div class="row">
            <div class="col-md-8">
                <h3>Task #3 Simulasi Jika Superhero Menikah</h3>
            </div>
            <div class="col-md-4  text-right">
                <button class="btn btn-primary">Edit</button>
                <button class="btn btn-danger">Hapus</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tbody><tr>
                        <td>Suami</td>
                        <td>
                            <select class="form-control">
                                <option value="1" selected="">Wolverine</option>
                                <option value="2">Professor X</option>
                                <option value="3">Beast</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Istri</td>
                        <td>
                            <select class="form-control">
                                <option value="p">Raven</option>
                                <option value="l" selected="">Storm</option>
                            </select>
                        </td>
                    </tr>
                </tbody></table>

                <h5>Maka Anaknya Kemungkinan Akan Memiliki Skill Berikut:</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr><th>No</th>
                    <th>Skill</th>
                    </tr></thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Bisa Terbang</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Bisa Membuat Hujan</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Bisa Membuat Petir</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Bisa Mengendalikan Angin dan Badai</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Bisa Sembuh Dengan Cepat</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Mempunyai Cakar Yang Lebih Kuat Dari Baja</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>